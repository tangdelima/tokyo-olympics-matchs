# README #

This Project is a little spring boot applucation to sharp up and demonstrate my knowledge in  Java, OO, TDD and  Spring Framework. 

### Dependencies ###
- [Apache Maven](https://maven.apache.org)
- Java 8


### How do I get set up? ###

- Access the Project Rio and run:
`mvn clean install`

### Running the project ###

- After installing the Project with maven. To start it run:
`java -jar target/tokyo-olympics-matchs-0.0.1-SNAPSHOT.jar`

- The url to list matches is: http://localhost:8080/match

- An example of request body to create a new match:
`    {
         "sport": "SOCCER",
         "place": {
             "id": 2
         },
         "stage": {
             "id": 1,
             "name": "Eliminatorias"
         },
         "player1": {
             "id": 1,
             "name": "Brazil"
         },
         "player2": {
             "id": 2,
             "name": "Argentina"
         },
         "start": 1515723702759,
         "end": 1515725562759
     }`

### Considerations ###

The following items have been taken out The scope to make the delivery possible:

- Input validation on payload to validate the minimum 30 minute match rule.
- The API Update(PUT) methods. 
- More Integration Tests

## To see more of my work check my github profile ##
https://github.com/tangdelima