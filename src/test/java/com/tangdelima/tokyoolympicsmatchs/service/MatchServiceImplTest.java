package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.exception.*;
import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.util.TestHelper;
import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;
import com.tangdelima.tokyoolympicsmatchs.validation.CreateMatchValidator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;
import java.util.List;

import static org.mockito.Matchers.any;

@RunWith(JUnit4.class)
public class MatchServiceImplTest {

    private long minimunMatchDuration = CreateMatchValidator.MINIMUM_MATCH_DURATION;

    public MatchServiceImplTest(){
        TransactionContext.setUp("tom-pu");
    }

    @Before
    public void beforeTest(){
        TestHelper.clearData();
        TestHelper.loadData();
    }

    @After
    public void afterTest() {
        TestHelper.clearData();
    }

    @Test
    public void cannotCreateMatchWithSamePlayers() {
        MatchService service = TestHelper.createMatchService();

        Player player = TestHelper.getPlayer1();

        Date start = new Date();
        Date end = new Date(start.getTime() + minimunMatchDuration + 1);

        List<Match> matchesFoundBefore = service.getMatches();

        Stage stage = new Stage();

        boolean hasException = false;

        try {
            service.createMatch(any(), start, end, any(), stage, player, player);
        } catch(SamePlayerException spe) {
            hasException = true;
        } catch (OlympicException e) {}

        List<Match> matchesFoundAfter = service.getMatches();

        Assert.assertTrue(hasException);
        Assert.assertEquals(matchesFoundAfter.size(), matchesFoundBefore.size());
    }

    @Test
    public void cannotCreateTwoMatchesOnTheSamePlaceAtTheSameTime() {
        MatchService service = TestHelper.createMatchService();

        Player player = TestHelper.getPlayer1();
        Player player2 = TestHelper.getPlayer2();
        Place place1 = TestHelper.getPlace();

        Date start = new Date();
        Date end = new Date(start.getTime() + minimunMatchDuration + 1);

        List<Match> matchesFoundBefore = service.getMatches();

        boolean hasException = false;

        try {
            service.createMatch(any(), start, end, place1, any(), player, player2);
            Assert.assertEquals(matchesFoundBefore.size() + 1, service.getMatches().size());
            service.createMatch(any(), start, end, place1, any(), player, player2);
        } catch(SamePlaceException spe) {
            hasException = true;
        } catch (OlympicException e) {}

        List<Match> matchesFoundAfter = service.getMatches();

        Assert.assertTrue(hasException);
        Assert.assertEquals(matchesFoundBefore.size()+1, matchesFoundAfter.size());
    }

    @Test
    public void cannotCreateMoreThanFourMatchesOnPlaceToday() {
        MatchService service = TestHelper.createMatchService();

        Player player = TestHelper.getPlayer1();

        Player player2 = TestHelper.getPlayer2();

        Place place1 = TestHelper.getPlace();

        Date start = new Date();
        Date end = new Date(start.getTime() + minimunMatchDuration + 1);

        List<Match> matchesFoundBefore = service.getMatches();

        boolean hasException = false;

        try {
            service.createMatch(any(), start, end, place1, any(), player, player2);
            start = end;
            end = new Date(start.getTime() + minimunMatchDuration + 1);
            service.createMatch(any(), start, end, place1, any(), player, player2);
            start = end;
            end = new Date(start.getTime() + minimunMatchDuration + 1);
            service.createMatch(any(), start, end, place1, any(), player, player2);
            start = end;
            end = new Date(start.getTime() + minimunMatchDuration + 1);
            service.createMatch(any(), start, end, place1, any(), player, player2);
            start = end;
            end = new Date(start.getTime() + minimunMatchDuration + 1);
            service.createMatch(any(), start, end, place1, any(), player, player2);
        } catch(TooMuchMatchesADayException spe) {
            hasException = true;
        } catch (OlympicException e) {}

        List<Match> matchesFoundAfter = service.getMatches();

        Assert.assertTrue(hasException);
        Assert.assertEquals(matchesFoundBefore.size()+4, matchesFoundAfter.size());

    }

    @Test
    public void cannotCreateMatchWithLessThan30Minutes() {
        MatchService service = TestHelper.createMatchService();

        Player player = TestHelper.getPlayer1();
        Player player2 = TestHelper.getPlayer2();
        Place place1 = TestHelper.getPlace();

        Date start = new Date();
        Date end = new Date(start.getTime() + minimunMatchDuration - 1);

        List<Match> matchesFoundBefore = service.getMatches();

        boolean hasException = false;

        try {
            service.createMatch(any(), start, end, place1, any(), player, player2);
        } catch(MinimumMatchTimeException spe) {
            hasException = true;
        } catch (OlympicException e) {}

        List<Match> matchesFoundAfter = service.getMatches();

        Assert.assertTrue(hasException);
        Assert.assertEquals(matchesFoundBefore.size(), matchesFoundAfter.size());


    }

    @Test
    public void canCreateMatch() throws OlympicException {
        MatchService service = TestHelper.createMatchService();

        Player player = TestHelper.getPlayer1();
        Player player2 = TestHelper.getPlayer2();
        Place place1 = TestHelper.getPlace();

        Date start = new Date();
        Date end = new Date(start.getTime() + minimunMatchDuration + 1);

        List<Match> matchesFoundBefore = service.getMatches();
        service.createMatch(any(), start, end, place1, any(), player, player2);
        List<Match> matchesFoundAfter = service.getMatches();

        Assert.assertEquals(matchesFoundBefore.size()+1, matchesFoundAfter.size());
    }

    @Test
    public void canCreateMatchWithSamePlayersOnSemifinals() throws OlympicException{
        MatchService service = TestHelper.createMatchService();

        Player player = TestHelper.getPlayer1();
        Stage stage = TestHelper.getSemifinalStage();
        Place place = TestHelper.getPlace();

        Date start = new Date();
        Date end = new Date(start.getTime() + minimunMatchDuration + 1);

        List<Match> matchesFoundBefore = service.getMatches();
        service.createMatch(any(), start, end, place, stage, player, player);
        List<Match> matchesFoundAfter = service.getMatches();

        Assert.assertEquals(matchesFoundBefore.size() + 1, matchesFoundAfter.size());
    }

    @Test
    public void canCreateMatchWithSamePlayersOnFinals() throws OlympicException{
        MatchService service = TestHelper.createMatchService();

        Player player = TestHelper.getPlayer1();
        Stage stage = TestHelper.getFinalStage();

        Date start = new Date();
        Date end = new Date(start.getTime() + minimunMatchDuration + 1);

        List<Match> matchesFoundBefore = service.getMatches();
        service.createMatch(any(), start, end, any(), stage, player, player);
        List<Match> matchesFoundAfter = service.getMatches();

        Assert.assertEquals(matchesFoundBefore.size() + 1, matchesFoundAfter.size());

    }

    @Test
    public void canListMatches() throws OlympicException{
        TestHelper.loadMatchs();
        MatchService service = TestHelper.createMatchService();
        List<Match> matchesFoundAfter = service.getMatches();
        Assert.assertTrue(matchesFoundAfter.size() > 0 );
    }

    @Test
    public void canListMatchesSortedByDate() {
        TestHelper.loadTemporalMatches();
        Match nowMatch = TestHelper.getNowMatch();
        Match futureMatch = TestHelper.getFutureMatch();
        Match pastMatch = TestHelper.getPastMatch();

        MatchService service = TestHelper.createMatchService();
        List<Match> matches = service.getMatches();

        Assert.assertEquals(pastMatch, matches.get(0));
        Assert.assertEquals(nowMatch, matches.get(1));
        Assert.assertEquals(futureMatch, matches.get(2));
    }

    @Test
    public void canListMatchesBySoccer() throws OlympicException{
        TestHelper.loadMatchs();
        MatchService service = TestHelper.createMatchService();
        List<Match> matchesFound = service.getMatchesBy(Sport.SOCCER);
        Assert.assertEquals(1, matchesFound.size());
    }

    @Test
    public void canListMatchesByBasketBall() throws OlympicException{
        TestHelper.loadMatchs();
        MatchService service = TestHelper.createMatchService();
        List<Match> matchesFoundAfter = service.getMatchesBy(Sport.BASKETBALL);
        Assert.assertEquals(1, matchesFoundAfter.size());
    }

    @Test
    public void canListMatchesByVolleyBall() throws OlympicException{
        TestHelper.loadMatchs();
        MatchService service = TestHelper.createMatchService();
        List<Match> matchesFoundAfter = service.getMatchesBy(Sport.VOLLEYBALL);
        Assert.assertEquals(1, matchesFoundAfter.size());
    }

    @Test
    public void canListMatchById() throws OlympicException{
        TestHelper.loadMatchs();
        MatchService service = TestHelper.createMatchService();
        Match match = TestHelper.getMatch();
        Match matchFound = service.getMatch(match.getId());
        Assert.assertEquals(match, matchFound);
    }

    @Test
    public void canRemoveMatch() throws OlympicException{
        TestHelper.loadMatchs();
        MatchService service = TestHelper.createMatchService();
        List<Match> matchesFoundBefore = service.getMatches();
        Match match = service.getMatch(TestHelper.getMatch().getId());
        service.removeMatch(match);
        List<Match> matchesFoundAfterRemove = service.getMatches();
        Assert.assertTrue(matchesFoundBefore.size() > matchesFoundAfterRemove.size());
    }
}
