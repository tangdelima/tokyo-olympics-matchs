package com.tangdelima.tokyoolympicsmatchs;

import com.tangdelima.tokyoolympicsmatchs.model.Match;
import com.tangdelima.tokyoolympicsmatchs.model.Sport;
import com.tangdelima.tokyoolympicsmatchs.util.TestHelper;
import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;
import com.tangdelima.tokyoolympicsmatchs.validation.CreateMatchValidator;
import com.tangdelima.tokyoolympicsmatchs.view.MatchVO;
import com.tangdelima.tokyoolympicsmatchs.view.PlaceVO;
import com.tangdelima.tokyoolympicsmatchs.view.PlayerVO;
import com.tangdelima.tokyoolympicsmatchs.view.StageVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TokyoOlympicsMatchsApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Before
	public void beforeTests() {
		TransactionContext.setUp("tom-pu");
		TestHelper.clearData();
		TestHelper.loadData();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void createMatch() {
		MatchVO matchVO = new MatchVO();
		matchVO.setSport(Sport.SOCCER.name());
		matchVO.setPlayer1(new PlayerVO(TestHelper.getPlayer1()));
		matchVO.setPlayer2(new PlayerVO(TestHelper.getPlayer2()));
		matchVO.setPlace(new PlaceVO(TestHelper.getPlace()));
		matchVO.setStage(new StageVO(TestHelper.getAnyStage()));
		matchVO.setStart(new Date());
		Date end = new Date(new Date().getTime() + CreateMatchValidator.MINIMUM_MATCH_DURATION + 1);
		matchVO.setEnd(end);

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		HttpEntity<MatchVO> request = new HttpEntity<>(matchVO, headers);
		ResponseEntity<Long> response = restTemplate.postForEntity("/match", request, Long.class);
		Assert.assertEquals(200, response.getStatusCodeValue());
		Assert.assertNotNull(response.getBody());
	}

	@Test
	public void findTestById() {
		TestHelper.loadMatchs();
		Match match = TestHelper.getMatch();
		Long idMatch = match.getId();

		ResponseEntity<Match> response = restTemplate.getForEntity("/match/" + idMatch, Match.class);

		Assert.assertEquals(200, response.getStatusCodeValue());
		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(match, response.getBody());
	}

	@Test
	public void findTests() {
		TestHelper.loadMatchs();


		ResponseEntity<Match[]> response = restTemplate.getForEntity("/match", Match[].class);

		Assert.assertEquals(200, response.getStatusCodeValue());
		Assert.assertNotNull(response.getBody());
		Assert.assertTrue(response.getBody().length > 0);
	}
}
