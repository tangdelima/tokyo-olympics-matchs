package com.tangdelima.tokyoolympicsmatchs.util;

import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAO;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAOImpl;
import com.tangdelima.tokyoolympicsmatchs.service.MatchServiceImpl;
import com.tangdelima.tokyoolympicsmatchs.validation.CreateMatchValidator;

import javax.persistence.EntityManager;
import java.util.Calendar;
import java.util.Date;

public class TestHelper {

    private static Player player1;
    private static Player player2;
    private static Place place;
    private static Stage anyStage;
    private static Stage semifinalStage;
    private static Stage finalStage;
    private static Match match;
    private static Match soccerMatch;
    private static Match basketBallMatch;
    private static Match volleyBallMatch;
    private static Match pastMatch;
    private static Match nowMatch;
    private static Match futureMatch;

    public static void clearData() {
        TransactionContext.beginTransaction();
        EntityManager em = TransactionContext.getEntityManager();
        em.createQuery("DELETE FROM Match").executeUpdate();
        em.createQuery("DELETE FROM Player").executeUpdate();
        em.createQuery("DELETE FROM Place").executeUpdate();
        em.createQuery("DELETE FROM Stage").executeUpdate();
        TransactionContext.commitTransaction();
    }

    public static MatchServiceImpl createMatchService() {
        MatchDAO dao = new MatchDAOImpl();
        CreateMatchValidator validator = new CreateMatchValidator(dao);
        return new MatchServiceImpl(dao, validator);
    }

    public static void loadData() {
        player1 = new Player();
        player1.setCountryName("Player 1");

        player2 = new Player();
        player2.setCountryName("Player 2");

        place = new Place();
        place.setName("Anywhere");

        anyStage = new Stage();
        anyStage.setName("Any Stage");

        semifinalStage = new Stage();
        semifinalStage.setName("SemiFinal");
        semifinalStage.setAllowSamePlayer(true);

        finalStage = new Stage();
        finalStage.setName("Final");
        finalStage.setAllowSamePlayer(true);

        TransactionContext.beginTransaction();
        EntityManager em = TransactionContext.getEntityManager();
        em.persist(player1);
        em.persist(player2);
        em.persist(place);
        em.persist(anyStage);
        em.persist(semifinalStage);
        em.persist(finalStage);
        TransactionContext.commitTransaction();
    }

    public static void loadMatchs() {
        match = new Match();
        match.setSport(Sport.SOCCER);
        match.setPlayer1(player1);
        match.setPlayer2(player2);
        match.setPlace(place);
        match.setStage(anyStage);
        match.setStartDate(new Date());

        soccerMatch = new Match();
        soccerMatch.setPlayer1(player1);
        soccerMatch.setPlayer2(player2);
        soccerMatch.setPlace(place);
        soccerMatch.setStage(anyStage);
        soccerMatch.setSport(Sport.SOCCER);
        soccerMatch.setStartDate(new Date());

        basketBallMatch = new Match();
        basketBallMatch.setPlayer1(player1);
        basketBallMatch.setPlayer2(player2);
        basketBallMatch.setPlace(place);
        basketBallMatch.setStage(anyStage);
        basketBallMatch.setSport(Sport.BASKETBALL);
        basketBallMatch.setStartDate(new Date());

        volleyBallMatch = new Match();
        volleyBallMatch.setPlayer1(player1);
        volleyBallMatch.setPlayer2(player2);
        volleyBallMatch.setPlace(place);
        volleyBallMatch.setStage(anyStage);
        volleyBallMatch.setSport(Sport.VOLLEYBALL);
        volleyBallMatch.setStartDate(new Date());

        TransactionContext.beginTransaction();
        EntityManager em = TransactionContext.getEntityManager();
        em.persist(match);
        em.persist(soccerMatch);
        em.persist(basketBallMatch);
        em.persist(volleyBallMatch);
        TransactionContext.commitTransaction();
    }

    public static void loadTemporalMatches() {
        Calendar calendar = Calendar.getInstance();

        nowMatch = new Match();
        nowMatch.setPlayer1(player1);
        nowMatch.setPlayer2(player2);
        nowMatch.setPlace(place);
        nowMatch.setStage(anyStage);
        nowMatch.setStartDate(new Date());
        nowMatch.setSport(Sport.SOCCER);

        calendar.set(3000, 01, 01);
        futureMatch = new Match();
        futureMatch.setPlayer1(player1);
        futureMatch.setPlayer2(player2);
        futureMatch.setPlace(place);
        futureMatch.setStage(anyStage);
        futureMatch.setStartDate(calendar.getTime());
        futureMatch.setSport(Sport.SOCCER);

        calendar.set(1990, 01, 01);
        pastMatch = new Match();
        pastMatch.setPlayer1(player1);
        pastMatch.setPlayer2(player2);
        pastMatch.setPlace(place);
        pastMatch.setStage(anyStage);
        pastMatch.setStartDate(calendar.getTime());
        pastMatch.setSport(Sport.SOCCER);

        TransactionContext.beginTransaction();
        EntityManager em = TransactionContext.getEntityManager();
        em.persist(nowMatch);
        em.persist(futureMatch);
        em.persist(pastMatch);
        TransactionContext.commitTransaction();
    }

    public static Player getPlayer1() {
        return player1;
    }

    public static Player getPlayer2() {
        return player2;
    }

    public static Place getPlace() {
        return place;
    }

    public static Stage getAnyStage() {
        return anyStage;
    }

    public static Stage getSemifinalStage() {
        return semifinalStage;
    }

    public static Stage getFinalStage() {
        return finalStage;
    }

    public static Match getMatch() {
        return match;
    }

    public static Match getPastMatch() {
        return pastMatch;
    }

    public static Match getNowMatch() {
        return nowMatch;
    }

    public static Match getFutureMatch() {
        return futureMatch;
    }
}
