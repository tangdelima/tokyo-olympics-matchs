package com.tangdelima.tokyoolympicsmatchs.validation;

import com.tangdelima.tokyoolympicsmatchs.exception.*;
import com.tangdelima.tokyoolympicsmatchs.model.Match;
import com.tangdelima.tokyoolympicsmatchs.model.Place;
import com.tangdelima.tokyoolympicsmatchs.model.Player;
import com.tangdelima.tokyoolympicsmatchs.model.Stage;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class CreateMatchValidator implements OlympicValidator {

    public static long MINIMUM_MATCH_DURATION = 30l*60l;

    private MatchDAO dao;

    public CreateMatchValidator () {}

    @Autowired
    public CreateMatchValidator(MatchDAO dao) {
        this.dao = dao;
    }

    public boolean isValid(Object object) throws OlympicException {
        Match match;

        if (!(object instanceof Match)) {
            throw  new IllegalArgumentException();
        } else {
            match = (Match) object;
        }

        Player player1 = match.getPlayer1();
        Player player2 = match.getPlayer2();
        Place place = match.getPlace();
        Stage stage = match.getStage();

        Date start = match.getStartDate();
        Date end = match.getEndDate();

        if (end.getTime() - start.getTime() < MINIMUM_MATCH_DURATION) {
            throw new MinimumMatchTimeException();
        }

        System.out.println(player1);
        System.out.println(player2);
        System.out.println(stage);
        if (player1.equals(player2) && !Boolean.TRUE.equals(stage.getAllowSamePlayer())) {
            throw new SamePlayerException();
        }

        List<Match> matches = dao.listMatches();
        long samePlaceMatchesFound = matches.stream().filter((m)->{
            return place.equals(m.getPlace()) &&
                    (m.getStartDate().getTime() == start.getTime() || m.getStartDate().after(start)) &&
                    (m.getEndDate().getTime() == end.getTime() || m.getEndDate().before(end));
        }).count();
        if(samePlaceMatchesFound > 0){
            throw new SamePlaceException();
        }

        int currentDate = getCalendarDate(start);
        long matchesAtPlaceToday = matches.stream().filter((m)->{
            return getCalendarDate(m.getStartDate()) == currentDate;
        }).count();
        if (matchesAtPlaceToday >= 4) {
            throw new TooMuchMatchesADayException();
        }

        return true;
    }

    private int getCalendarDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DATE);
    }
}
