package com.tangdelima.tokyoolympicsmatchs.validation;

import com.tangdelima.tokyoolympicsmatchs.exception.OlympicException;

public interface OlympicValidator {

    public boolean isValid(Object object) throws OlympicException;

}
