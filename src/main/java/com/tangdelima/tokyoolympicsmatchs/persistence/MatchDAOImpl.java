package com.tangdelima.tokyoolympicsmatchs.persistence;

import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

@Service
public class MatchDAOImpl implements MatchDAO {

    @Override
    public void persistMatch(Match match) {
        try {
            TransactionContext.beginTransaction();
            TransactionContext.getEntityManager().persist(match);
            TransactionContext.commitTransaction();
        } catch(PersistenceException e){
            TransactionContext.rollbackTransaction();
        }
    }

    @Override
    public List<Match> listMatches() {
        EntityManager em = TransactionContext.getEntityManager();
        TypedQuery<Match> query = em.createQuery("FROM Match", Match.class);
        return query.getResultList();
    }

    @Override
    public List<Match> listMatches(Sport filter) {
        EntityManager em = TransactionContext.getEntityManager();
        TypedQuery<Match> query = em.createQuery("FROM Match WHERE sport = :sport", Match.class);
        query.setParameter("sport", filter);
        return query.getResultList();
    }

    @Override
    public Match findMatch(Long idMatch) {
        EntityManager em = TransactionContext.getEntityManager();
        return em.find(Match.class, idMatch);
    }

    @Override
    public Match updateMatch(Match match) {
        return null;
    }

    @Override
    public void deleteMatch(Match match) {
        TransactionContext.beginTransaction();
        EntityManager em = TransactionContext.getEntityManager();
        em.remove(match);
        TransactionContext.commitTransaction();
    }

}
