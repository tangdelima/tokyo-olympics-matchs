package com.tangdelima.tokyoolympicsmatchs.persistence;

import com.tangdelima.tokyoolympicsmatchs.model.Place;

public interface PlaceDAO {

    public Place findPlace(Long idPlace);

}
