package com.tangdelima.tokyoolympicsmatchs.persistence;

import com.tangdelima.tokyoolympicsmatchs.model.Place;
import com.tangdelima.tokyoolympicsmatchs.model.Player;
import com.tangdelima.tokyoolympicsmatchs.model.Stage;
import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;
import org.springframework.stereotype.Service;

@Service
public class PlayerDAOImpl implements PlayerDAO {
    public Player findPlayer(Long idPlayer) {
        return TransactionContext.getEntityManager().find(Player.class, idPlayer);
    }
}
