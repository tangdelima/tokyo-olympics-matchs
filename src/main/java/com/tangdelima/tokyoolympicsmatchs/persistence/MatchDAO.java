package com.tangdelima.tokyoolympicsmatchs.persistence;


import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;

import java.util.Date;
import java.util.List;

public interface MatchDAO {

    public void persistMatch(Match match);
    public List<Match> listMatches();
    public List<Match> listMatches(Sport filter);
    public Match findMatch(Long idMatch);
    public Match updateMatch(Match match);
    public void deleteMatch(Match match);

}
