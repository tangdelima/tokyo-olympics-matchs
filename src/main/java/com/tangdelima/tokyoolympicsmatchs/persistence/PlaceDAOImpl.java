package com.tangdelima.tokyoolympicsmatchs.persistence;

import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class PlaceDAOImpl implements PlaceDAO {

    public Place findPlace(Long idPlace) {
        return TransactionContext.getEntityManager().find(Place.class, idPlace);
    }

}
