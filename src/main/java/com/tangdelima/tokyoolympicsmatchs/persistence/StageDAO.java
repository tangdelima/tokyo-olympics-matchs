package com.tangdelima.tokyoolympicsmatchs.persistence;

import com.tangdelima.tokyoolympicsmatchs.model.Place;
import com.tangdelima.tokyoolympicsmatchs.model.Player;
import com.tangdelima.tokyoolympicsmatchs.model.Stage;
import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;

public interface StageDAO {

    public Stage findStage(Long idStage);

}
