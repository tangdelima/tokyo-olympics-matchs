package com.tangdelima.tokyoolympicsmatchs.controller;

import com.tangdelima.tokyoolympicsmatchs.exception.OlympicException;
import com.tangdelima.tokyoolympicsmatchs.facade.MatchFacade;
import com.tangdelima.tokyoolympicsmatchs.model.Match;
import com.tangdelima.tokyoolympicsmatchs.model.Sport;
import com.tangdelima.tokyoolympicsmatchs.service.MatchService;
import com.tangdelima.tokyoolympicsmatchs.view.MatchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/match")
@RestController
public class MatchController {

    @Autowired
    private MatchService service;

    @Autowired
    private MatchFacade facade;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MatchVO> getMatches() {
        return facade.listVOs(service.getMatches());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MatchVO getMatch(@PathVariable("id") Long idMatch) {
        return facade.getMatch(idMatch);
    }

    @RequestMapping(params = {"sport"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MatchVO> getMatches(@RequestParam("sport") String sport) {
        return facade.listVOs(service.getMatchesBy(Sport.valueOf(sport)));
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Long postMatch(@RequestBody @Valid MatchVO match) throws OlympicException {
        return facade.createMatch(match).getId();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String deleteMatch(@PathVariable("id") Long idMatch) {
        Match match = service.getMatch(idMatch);
        if (match == null) {
            return "404";
        }

        try {
            service.removeMatch(match);
            return "OK";
        } catch (Exception e) {
            return e.toString() + e.getMessage();
        }
    }
}
