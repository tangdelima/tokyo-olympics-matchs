package com.tangdelima.tokyoolympicsmatchs.facade;

import com.tangdelima.tokyoolympicsmatchs.exception.OlympicException;
import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.service.*;
import com.tangdelima.tokyoolympicsmatchs.view.MatchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MatchFacade {

    private MatchService matchService;
    private PlayerService playerService;
    private PlaceService placeService;
    private StageService stageService;

    public MatchFacade() {}

    @Autowired
    public MatchFacade(MatchService matchService,
                       PlayerService playerService,
                       PlaceService placeService,
                       StageService stageService) {
        this.matchService = matchService;
        this.playerService = playerService;
        this.placeService = placeService;
        this.stageService = stageService;
    }

    public Match createMatch(MatchVO vo) throws OlympicException {
        Sport sport = Sport.valueOf(vo.getSport());
        Place place = placeService.getPlace(Long.valueOf(vo.getPlace().getId()));
        Stage stage = stageService.getStage(Long.valueOf(vo.getStage().getId()));

        Player player1 = playerService.getPlayer(Long.valueOf(vo.getPlayer1().getId()));
        Player player2 = playerService.getPlayer(Long.valueOf(vo.getPlayer2().getId()));

        return matchService.createMatch(
                sport,
                vo.getStart(),
                vo.getEnd(),
                place,
                stage,
                player1,
                player2);

    }

    public List<MatchVO> listVOs(List<Match> matchList) {
        return matchList.stream().map(m->new MatchVO(m)).collect(Collectors.toList());
    }

    public MatchVO getMatch(Long idMatch) {
        return new MatchVO(matchService.getMatch(idMatch));
    }

}
