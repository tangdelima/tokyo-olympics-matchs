package com.tangdelima.tokyoolympicsmatchs;

import com.tangdelima.tokyoolympicsmatchs.util.TransactionContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.Persistence;

@SpringBootApplication
public class TokyoOlympicsMatchsApplication {

	public static void main(String[] args) {
		TransactionContext.setUp("tom-pu");
		SpringApplication.run(TokyoOlympicsMatchsApplication.class, args);
	}
}
