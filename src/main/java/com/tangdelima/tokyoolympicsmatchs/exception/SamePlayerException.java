package com.tangdelima.tokyoolympicsmatchs.exception;

public class SamePlayerException extends OlympicException {

    @Override
    public String getMessage() {
        return "The same player cannot play this match. Same player matches are only allowed in SemiFinal o Final stages";
    }
}
