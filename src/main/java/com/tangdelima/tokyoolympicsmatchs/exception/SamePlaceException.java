package com.tangdelima.tokyoolympicsmatchs.exception;

public class SamePlaceException extends OlympicException {

    @Override
    public String getMessage() {
        return "Cannot create a match in a place already taken.";
    }
}
