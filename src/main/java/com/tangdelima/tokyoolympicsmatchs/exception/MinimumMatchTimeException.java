package com.tangdelima.tokyoolympicsmatchs.exception;

public class MinimumMatchTimeException extends OlympicException {

    @Override
    public String getMessage() {
        return "A match cannot last less than 30 minutes.";
    }
}
