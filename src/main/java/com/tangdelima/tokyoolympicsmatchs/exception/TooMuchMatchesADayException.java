package com.tangdelima.tokyoolympicsmatchs.exception;

public class TooMuchMatchesADayException extends OlympicException {

    @Override
    public String getMessage() {
        return "This place has too many matches for today. Try another place or another day.";
    }
}
