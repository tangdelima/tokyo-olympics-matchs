package com.tangdelima.tokyoolympicsmatchs.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Match")
public class Match {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "sport")
    @Enumerated(EnumType.STRING)
    private Sport sport;

    @ManyToOne
    @JoinColumn(name = "id_place", referencedColumnName = "id")
    private Place place;

    @Column(name = "start")
    private Date startDate;

    @Column(name = "end")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "id_player1", referencedColumnName = "id")
    private Player player1;

    @ManyToOne
    @JoinColumn(name = "id_player2", referencedColumnName = "id")
    private Player player2;

    @ManyToOne
    @JoinColumn(name = "id_stage",  referencedColumnName = "id")
    private Stage stage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Match)) return false;
        Match match = (Match) o;
        return Objects.equals(getId(), match.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }
}
