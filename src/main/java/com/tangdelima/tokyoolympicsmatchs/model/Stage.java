package com.tangdelima.tokyoolympicsmatchs.model;

import javax.persistence.*;

@Entity
@Table(name = "Stage")
public class Stage {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "allow_same_player")
    private  Boolean allowSamePlayer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAllowSamePlayer() {
        return allowSamePlayer;
    }

    public void setAllowSamePlayer(Boolean allowSamePlayer) {
        this.allowSamePlayer = allowSamePlayer;
    }
}
