package com.tangdelima.tokyoolympicsmatchs.model;

public enum Sport {

    SOCCER, BASKETBALL, VOLLEYBALL;

}
