package com.tangdelima.tokyoolympicsmatchs.view;

import com.tangdelima.tokyoolympicsmatchs.model.Match;
import com.tangdelima.tokyoolympicsmatchs.model.Player;

import javax.validation.constraints.NotNull;

public class PlayerVO {

    @NotNull
    private int id;

    private String name;

    public PlayerVO() {}

    public PlayerVO(Player player){
        this.id = player.getId().intValue();
        this.name = player.getCountryName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
