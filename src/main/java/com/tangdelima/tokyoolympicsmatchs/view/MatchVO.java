package com.tangdelima.tokyoolympicsmatchs.view;

import com.tangdelima.tokyoolympicsmatchs.model.Match;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class MatchVO {

    private Long id;

    @NotNull
    private String sport;

    @NotNull
    private PlaceVO place;

    @NotNull
    private StageVO stage;

    @NotNull
    private PlayerVO player1;

    @NotNull
    private PlayerVO player2;

    @NotNull
    private Date start;

    @NotNull
    private Date end;

    public MatchVO() {}

    public MatchVO(Match match) {
        this.id = match.getId();
        this.sport = match.getSport().name();
        this.place = new PlaceVO(match.getPlace());
        this.stage = new StageVO(match.getStage());
        this.player1 = new PlayerVO(match.getPlayer1());
        this.player2 = new PlayerVO(match.getPlayer2());
        this.start = match.getStartDate();
        this.end = match.getEndDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public PlaceVO getPlace() {
        return place;
    }

    public void setPlace(PlaceVO place) {
        this.place = place;
    }

    public StageVO getStage() {
        return stage;
    }

    public void setStage(StageVO stage) {
        this.stage = stage;
    }

    public PlayerVO getPlayer1() {
        return player1;
    }

    public void setPlayer1(PlayerVO player1) {
        this.player1 = player1;
    }

    public PlayerVO getPlayer2() {
        return player2;
    }

    public void setPlayer2(PlayerVO player2) {
        this.player2 = player2;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
