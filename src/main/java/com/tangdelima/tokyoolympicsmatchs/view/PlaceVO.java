package com.tangdelima.tokyoolympicsmatchs.view;

import com.tangdelima.tokyoolympicsmatchs.model.Place;

import javax.validation.constraints.NotNull;

public class PlaceVO {

    @NotNull
    private int id;

    private String name;

    public PlaceVO() {}

    public PlaceVO(Place place) {
        this.id = place.getId().intValue();
        this.name = place.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
