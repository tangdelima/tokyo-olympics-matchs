package com.tangdelima.tokyoolympicsmatchs.view;

import com.tangdelima.tokyoolympicsmatchs.model.Stage;

import javax.validation.constraints.NotNull;

public class StageVO {

    @NotNull
    private int id;

    private String name;

    public StageVO() {}

    public StageVO(Stage stage) {
        this.id = stage.getId().intValue();
        this.name = stage.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
