package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.exception.*;
import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAO;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAOImpl;
import com.tangdelima.tokyoolympicsmatchs.persistence.PlayerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {

    private PlayerDAO dao;

    @Autowired
    public PlayerServiceImpl(PlayerDAO dao) {
        this.dao = dao;
    }

    public Player getPlayer(Long idPlayer) {
        return dao.findPlayer(idPlayer);
    }
}