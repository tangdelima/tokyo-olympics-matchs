package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.exception.*;
import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAO;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAOImpl;
import com.tangdelima.tokyoolympicsmatchs.persistence.StageDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class StageServiceImpl implements StageService {

    private StageDAO dao;

    @Autowired
    public StageServiceImpl(StageDAO dao) {
        this.dao = dao;
    }

    public Stage getStage(Long idStage) {
        return dao.findStage(idStage);
    }

}