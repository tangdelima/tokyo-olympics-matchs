package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.exception.OlympicException;
import com.tangdelima.tokyoolympicsmatchs.exception.SamePlaceException;
import com.tangdelima.tokyoolympicsmatchs.exception.SamePlayerException;
import com.tangdelima.tokyoolympicsmatchs.model.*;

import java.util.Date;
import java.util.List;

public interface MatchService {

    public Match createMatch(Sport sport, Date start, Date end, Place place, Stage stage, Player player1, Player player2) throws SamePlayerException, SamePlaceException, OlympicException;
    public List<Match> getMatches();
    public List<Match> getMatchesBy(Sport sport);
    public Match getMatch(Long idMatch);
    public void removeMatch(Match match);

}
