package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.model.Place;
import com.tangdelima.tokyoolympicsmatchs.persistence.PlaceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaceServiceImpl implements PlaceService {

    private PlaceDAO dao;

    @Autowired
    public PlaceServiceImpl(PlaceDAO dao) {
        this.dao = dao;
    }

    public Place getPlace(Long idPlace) {
        return dao.findPlace(idPlace);
    }

}