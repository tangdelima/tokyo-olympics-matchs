package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.exception.OlympicException;
import com.tangdelima.tokyoolympicsmatchs.exception.SamePlaceException;
import com.tangdelima.tokyoolympicsmatchs.exception.SamePlayerException;
import com.tangdelima.tokyoolympicsmatchs.model.*;

import java.util.Date;
import java.util.List;

public interface PlaceService {

    public Place getPlace(Long idPlace);

}
