package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.model.Player;

public interface PlayerService {

    public Player getPlayer(Long idPlayer);

}
