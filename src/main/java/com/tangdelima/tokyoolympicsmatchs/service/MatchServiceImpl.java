package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.exception.OlympicException;
import com.tangdelima.tokyoolympicsmatchs.model.*;
import com.tangdelima.tokyoolympicsmatchs.persistence.MatchDAO;
import com.tangdelima.tokyoolympicsmatchs.validation.OlympicValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MatchServiceImpl implements MatchService {

    private MatchDAO dao;
    private OlympicValidator validator;

    public MatchServiceImpl() {}

    @Autowired
    public MatchServiceImpl(MatchDAO dao,
                            @Qualifier("createMatchValidator") OlympicValidator validator) {
        this.dao = dao;
        this.validator = validator;
    }

    @Override
    public Match createMatch(Sport sport, Date start, Date end, Place place, Stage stage, Player player1, Player player2) throws OlympicException {
        Match match = new Match();
        match.setSport(sport);
        match.setStartDate(start);
        match.setEndDate(end);
        match.setPlace(place);
        match.setStage(stage);
        match.setPlayer1(player1);
        match.setPlayer2(player2);

        if (validator.isValid(match)) {
            dao.persistMatch(match);
        }
        return match;
    }

    @Override
    public List<Match> getMatches() {
        return dao.listMatches().stream().sorted(Comparator.comparing(Match::getStartDate)).collect(Collectors.toList());
    }

    @Override
    public List<Match> getMatchesBy(Sport sport) {
        return dao.listMatches(sport);
    }

    @Override
    public Match getMatch(Long idMatch) {
        return dao.findMatch(idMatch);
    }

    @Override
    public void removeMatch(Match match) {
        dao.deleteMatch(match);
    }
}