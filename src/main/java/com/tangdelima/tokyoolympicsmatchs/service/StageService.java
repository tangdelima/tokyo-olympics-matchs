package com.tangdelima.tokyoolympicsmatchs.service;

import com.tangdelima.tokyoolympicsmatchs.model.Stage;

public interface StageService {

    public Stage getStage(Long idStage);

}
