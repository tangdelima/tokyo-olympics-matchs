package com.tangdelima.tokyoolympicsmatchs.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TransactionContext {

    private static EntityManagerFactory emf;
    private static EntityManager em = null;

    public static void setUp(String persistenceUnitName) {
        emf = Persistence.createEntityManagerFactory(persistenceUnitName);
    }

    public static EntityManager getEntityManager() {
        if (em == null) {
            em = emf.createEntityManager();
        }
        return em;
    }

    public static void beginTransaction() {
        EntityManager em = getEntityManager();
        if(!em.getTransaction().isActive()) {
            getEntityManager().getTransaction().begin();
        }
    }

    public static void commitTransaction() {
        getEntityManager().getTransaction().commit();
        em.close();
        em = null;
    }

    public static void rollbackTransaction() {
        getEntityManager().getTransaction().rollback();
        em.close();
        em = null;
    }

}
